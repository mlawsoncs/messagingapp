(function() {
  'use strict';

  angular.module('msgApp', [
      'msgApp.core',
      'msgApp.login',
      'msgApp.home',
      'msgApp.socket'
    ])
  .run(preventRouteOnDownload)
  .filter('trusted', TrustedFilter);

  preventRouteOnDownload.$inject = ['$rootScope', '$window'];

  function preventRouteOnDownload($rootScope, $window) {
    $rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl) {
      if (newUrl.indexOf('/download') !== -1) {
        event.preventDefault();
        $window.location.href = newUrl;
      }
    });
  }  

  TrustedFilter.$inject = ['$sce'];
  function TrustedFilter($sce) {
    return function(text) {
      return $sce.trustAsHtml(text);
    };
  }

})();