(function() {
  angular.module('msgApp.home')
    .service('ChatService', ChatService);

  ChatService.$inject = ['$http', 'Session'];
  function ChatService($http, Session) {
    return {
      getUserHistory: getUserHistory,
      getChannelHistory: getChannelHistory,
      createChannel: createChannel
    };

    function getUserHistory(fromUserId, toUserId) {
      return $http.post('/user/chat', {
        userId: fromUserId,
        toUserId: toUserId,
        sessionId: Session.sessionId
      }).then(function(results) {
        return results.data;
      });
    }

    function getChannelHistory(channelName) {
      return $http.post('/channel/chat', {
        channelName: channelName,
        userId: Session.userId,
        sessionId: Session.sessionId
      }).then(function(results) {
        return results.data;
      });
    }

    function createChannel(channelName) {
      return $http.post('/channel', {
        channelName: channelName,
        userId: Session.userId,
        sessionId: Session.sessionId
      }).then(function(results) {
        return results.data;
      });
    }
  }

})();