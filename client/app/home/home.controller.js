(function() {
  'use strict';

  angular.module('msgApp.home')
    .controller('HomeController',HomeController);

  HomeController.$inject = ['$state', 'AuthService', 'ChatService', 'Session', 'Socket', 'ngDialog', 'toastr'];
  function HomeController($state, AuthService, ChatService, Session, socket, ngDialog, toastr) {
    var vm = this;
    init();
    
    // Begin Socket handling

    // Get users
    socket.on('updateUsers', function(users) {
      console.log('users updated', users);
      vm.users = users;
    });

    // Get channels
    socket.on('updateChannels', function(channels) {
      console.log('channels updated', channels);
      vm.channels = channels;
    });

    // TODO: Handle channel users

    // Direct message handling
    socket.on('dm', function(message) {
      if (vm.activeChat.id === message.userId && vm.activeChat.type === 'direct') {
        vm.messages.unshift(message);
      } else {
        var user = findUser(message.userId);
        user.unread = true;
      }
    });

    // Channel message handling
    socket.on('cm', function(message) {
      if (message.nickname === Session.nickname)
        return;
      vm.messages.unshift(message);
    });

    // End socket handling

    // Begin handling Upload file modal window
    vm.uploadFile = function() {
      ngDialog.open({
        templateUrl: 'app/home/modals/fileUpload.template.html',
        controller: 'FileUploadController',
        controllerAs: 'vm'
      }).closePromise.then(function(data) {
        var result = data.value;

        if (typeof result !== 'object')
          return;
        var filename = result.filename;
        var recordId = result.recordId;

        uploadFileNotify(filename, recordId);
      });
    };

    function uploadFileNotify(filename, recordId) {
      if (vm.activeChat.type === 'direct') {
        socket.emit('dm-upload', {
          userId: vm.activeChat.id,
          recordId: recordId,
          filename: filename
        });
      } else {
        // Channel handling
        socket.emit('cm-upload', {
          channel: vm.activeChat.id,
          recordId: recordId,
          filename: filename
        });
      }
      var message = {
          userId: Session.userId,
          nickname: Session.nickname,
          timestamp: Date.now(),
          fileUpload: true
        };
      message.fileMessage = 'File shared: <a href="/download/' + recordId + '">' + filename + '</a>';
      vm.messages.unshift(message);
    }

    vm.prepareChannel = function() {
       ngDialog.open({
        templateUrl: 'app/home/modals/channelCreate.template.html',
        controller: 'ChannelCreateController',
        controllerAs: 'vm'
      }).closePromise.then(function(data) {
        console.log(data);
        if (typeof data.value !== 'object')
          return;
        socket.emit('channelCreated', { channel: data.value.channelName });
        toastr.success('Channel created', 'You did it!');
      });
    };

    vm.createChannel = function() {
      vm.creatingChannel = false;
      if (vm.newChannelName === '')
        return;
      ChatService.createChannel(vm.newChannelName)
        .then(function() {
          // Channel successfully created
          socket.emit('channelCreated', { channel: vm.newChannelName });
          vm.newChannelName = '';
        });
    };

    vm.changeChannel = function(channel) {
      if (vm.activeChat.id === channel.ChannelName)
        return;

      leaveCurrentChannels();

      vm.activeChat = {
        id: channel.ChannelName,
        name: channel.ChannelName,
        type: 'channel'
      };
      vm.messages = [];

      socket.emit('join', { channel: channel.ChannelName });

      ChatService.getChannelHistory(channel.ChannelName)
        .then(function(data) {
          vm.messages = data;
        });
    };

    vm.changeUser = function(user) {
      if (user.userId === Session.userId) {
        toastr.warning('Trying to message yourself, huh?','You can\'t do that');
        return;
      }
      leaveCurrentChannels();
      user.unread = false;

      vm.activeChat = {
        id: user.userId,
        name: user.nickname,
        type: 'direct'
      };
      vm.messages = [];

      ChatService.getUserHistory(Session.userId, user.userId)
        .then(function(data) {
          vm.messages = data;
        });
    };

    vm.sendMessage = function() {
      if (vm.newMessage === '')
        return;

      if (vm.activeChat.id === -1) {
        vm.newMessage = '';
        return;
      }

      if (vm.activeChat.type === 'direct') {
        socket.emit('dm', {
          userId: vm.activeChat.id,
          message: vm.newMessage
        });
      } else {
        socket.emit('cm', {
          channel: vm.activeChat.id,
          message: vm.newMessage
        });
      }

      vm.messages.unshift({
        userId: Session.userId,
        nickname: Session.nickname,
        timestamp: Date.now(),
        message: vm.newMessage
      });
      vm.newMessage = '';  
    };

    vm.logout = function() {
      Session.destroy();
      socket.destroy();
      $state.go('login');
    };

    function findUser(userId) {
      var index = vm.users.map(function(u) { return u.userId; }).indexOf(userId);
      return vm.users[index];
    }

    function leaveCurrentChannels() {
      if (vm.activeChat.type !== 'channel')
        return;

      socket.emit('leave', { channel: vm.activeChat.id });
    }

    function init() {
      vm.activeChat = {
        id: -1,
        name: 'No chat is active',
        type: 'none'
      };
      vm.online = false;
      vm.newMessage = '';

      if (!AuthService.isAuthenticated()) {
        console.log('not authenticated!');
        $state.go('login');
        return;
      }

      socket.init();
      socket.on('connect', function() {
        console.log('connected');
        vm.online = true;
        socket.emit('authentication', { userId: Session.userId, sessionId: Session.sessionId });
      });
      socket.on('unauthorized', function(err) {
        toastr.error('Your session could not be authorized, please log back in','You have been logged off');
        Session.destroy();
        $state.go('login');
      });
      socket.on('disconnect', function(err) {
        vm.online = false;
      });
      socket.on('authenticated', function(data) {
        console.log('success!');
      });
    }

    function isNumeric(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }
  }
})();