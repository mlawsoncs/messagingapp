(function() {
  'use strict';

  angular.module('msgApp.home')
    .controller('FileUploadController', FileUploadController);

  FileUploadController.$inject = ['$http', '$scope', 'FileUploader'];
  function FileUploadController($http, $scope, FileUploader) {
    var vm = this;

    var uploader = vm.uploader = new FileUploader({
      url: '/upload'
    });

    uploader.onBeforeUploadItem = function(item) {
      console.log('in before');
    };

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
      console.log('in success');
      console.log('response',response);
      $scope.closeThisDialog({ recordId: response.recordId, filename: fileItem.file.name });
    };

    uploader.onErrorItem = function(fileItem, response, status, headers) {
      console.log('in error');
    };
  }
})();