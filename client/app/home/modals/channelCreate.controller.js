(function() {
  'use strict';

  angular.module('msgApp.home')
    .controller('ChannelCreateController', ChannelCreateController);

  ChannelCreateController.$inject = ['$http', '$scope', 'ChatService', 'toastr'];
  function ChannelCreateController($http, $scope, ChatService, toastr) {
    var vm = this;

    vm.createChannel = function() {
      ChatService.createChannel(vm.channelName)
        .then(function() {
          $scope.closeThisDialog({ channelName: vm.channelName });
        })
        .catch(function(err) {
          console.log('error:', err);
          var message = err.data && err.data.message ? err.data.message : 'An unknown error has occurred';
          toastr.error(message,'Uh oh');
        });
    };
  }
})();