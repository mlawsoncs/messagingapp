(function() {
  'use strict';

  angular.module('msgApp.home', [
      'msgApp.core',
      'ngDialog',
      'angularFileUpload',
      'toastr'
    ]);
})();