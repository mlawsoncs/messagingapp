(function() {
  'use strict';

  angular
    .module('msgApp.home')
    .run(appRun);

    appRun.$inject = ['routerHelper'];
    function appRun(routerHelper) {
      routerHelper.configureStates(getStates());
    }

    function getStates() {
      return [
        {
          state: 'home',
          config: {
            url: '/home',
            templateUrl: 'app/home/home.template.html',
            controller: 'HomeController',
            controllerAs: 'vm'
          }
        }
      ];
    }
    
})();