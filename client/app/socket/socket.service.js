(function() {
  'use strict';

  angular.module('msgApp.socket')
    .factory('Socket',Socket);

  Socket.$inject = ['$rootScope'];
  function Socket($rootScope) {
    var socket;
    var existingEvents = [];
    var isDestroying = false;
    return {
      on: function (eventName, callback) {
        existingEvents.push(eventName);
        socket.on(eventName, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            callback.apply(socket, args);
          });
        });
      },
      emit: function (eventName, data, callback) {
        socket.emit(eventName, data, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            if (callback) {
              callback.apply(socket, args);
            }
          });
        });
      },
      init: function() {
        console.log('in init');
        socket = io.connect({
          'force new connection': true
        });
      },
      destroy: function() {
        isDestroying = true;
        existingEvents.forEach(function(event) {
          socket.removeEventListener(event);
        });
        socket.disconnect();
        socket.close();
        socket = null;
      }
    };
  }
})();