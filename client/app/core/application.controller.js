(function() {
  'use strict';

  angular
    .module('msgApp.core')
    .controller('ApplicationController', ApplicationController);

  ApplicationController.$inject = ['$cookies', 'Session'];
  function ApplicationController($cookies, Session) {

    var cookie = $cookies.get('SESSION_VARIABLE');
    
    if (cookie) {
      var sessionVariable = JSON.parse(cookie);
      Session.create(sessionVariable.sessionId, sessionVariable.userId, sessionVariable.nickname);
    }
  }
})();