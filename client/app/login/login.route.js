(function() {
  'use strict';

  angular
    .module('msgApp.login')
    .run(appRun);

    appRun.$inject = ['routerHelper'];
    function appRun(routerHelper) {
      routerHelper.configureStates(getStates());
    }

    function getStates() {
      return [
        {
          state: 'root',
          config: {
            url: '/',
            templateUrl: 'app/login/login.template.html',
            controller: 'LoginController',
            controllerAs: 'vm'
          }
        },
        {
          state: 'login',
          config: {
            url: '/login',
            templateUrl: 'app/login/login.template.html',
            controller: 'LoginController',
            controllerAs: 'vm'
          }
        },
        {
          state: 'register',
          config: {
            url: '/register',
            templateUrl: 'app/login/register.template.html',
            controller: 'RegisterController',
            controllerAs: 'vm'
          }
        }
      ];
    }
    
})();