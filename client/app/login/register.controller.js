(function() {
  'use strict';

  angular.module('msgApp.login')
    .controller('RegisterController', RegisterController);

  RegisterController.$inject = ['$state', 'AuthService', 'toastr'];
  function RegisterController($state, AuthService, toastr) {
    var vm = this;

    vm.register = function(credentials) {
      AuthService.register(credentials)
        .then(function(user) {
          toastr.success('You have been registered successfully!', 'Welcome!');
          $state.go('home');
          return user;
        })
        .catch(function(err) {
          if (err.data && err.data.message) {
            toastr.warning('Could not register: ' + err.data.message,'Registration Failure');
          } else {
            toastr.warning('Could not register: Unknown Failure', 'Registration Failure');
          }
        });
    };
  }
})();