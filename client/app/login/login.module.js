(function() {
  'use strict';

  angular.module('msgApp.login', [
    'msgApp.core',
    'ngCookies',
    'ngAnimate',
    'toastr'
  ]);
})();