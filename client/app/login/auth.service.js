(function() {
  'use strict';

  angular.module('msgApp.login')
    .factory('AuthService', AuthService);

  AuthService.$inject = ['$http', 'Session'];

  function AuthService($http, Session) {
    var authService = {};

    authService.login = function(credentials) {
      console.log('login called..');
      return $http
        .post('/login', credentials)
        .then(function(res) {
          var user = res.data.user;
          console.log(user);
          Session.create(user.sessionId, user.userId, user.nickname);
          console.log('isAuthenticated:',!!Session.userId);
          return user;
        });
    };

    authService.register = function(credentials) {
      return $http
        .post('/register', credentials)
        .then(function(res) {
          console.log(res);
          var user = res.data.user;
          console.log(user);
          Session.create(user.sessionId, user.userId, user.nickname);
          console.log('isAuthenticated:',!!Session.userId);
          return user;
        });
    };

    authService.isAuthenticated = function() {
      console.log('is authenticated called');
      return !!Session.userId;
    };

    return authService;
  }

})();