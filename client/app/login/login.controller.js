(function() {
  'use strict';

  angular
    .module('msgApp.login')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['$state', 'AuthService', 'toastr'];
  function LoginController($state, AuthService, toastr) {
    var vm = this;
    this.credentials = {};

    if (AuthService.isAuthenticated()) {
      $state.go('home');
    }

    this.login = function(credentials) {
      AuthService.login(credentials)
        .then(function(user) {
          $state.go('home');
        },
        function(error) {
          if (error.data && error.data.message) {
            toastr.error(error.data.message, 'Could not log in');
          } else {
            toastr.error('An unexpected error has occurred. Please try again', 'Could not log in');
          }
        });
    };
  }

})();