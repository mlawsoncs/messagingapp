(function() {
  'use strict';

  angular.module('msgApp.login')
    .service('Session', Session);

  Session.$inject = [ '$cookies' ];
  function Session($cookies) {

    this.create = function createSession(sessionId, userId, nickname) {
      this.sessionId = sessionId;
      this.userId = userId;
      this.nickname = nickname;
      var cookie = JSON.stringify({ sessionId: sessionId, userId: userId, nickname: nickname });
      $cookies.put('SESSION_VARIABLE', cookie);
    };

    this.destroy = function destroySession() {
      this.sessionId = null;
      this.userId = null;
      $cookies.remove('SESSION_VARIABLE');
    };
  }
})();