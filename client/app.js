(function() {
  'use strict';

  angular.module('msgApp', [
      'msgApp.core',
      'msgApp.login',
      'msgApp.home',
      'msgApp.socket'
    ])
  .run(preventRouteOnDownload)
  .filter('trusted', TrustedFilter);

  preventRouteOnDownload.$inject = ['$rootScope', '$window'];

  function preventRouteOnDownload($rootScope, $window) {
    $rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl) {
      if (newUrl.indexOf('/download') !== -1) {
        event.preventDefault();
        $window.location.href = newUrl;
      }
    });
  }  

  TrustedFilter.$inject = ['$sce'];
  function TrustedFilter($sce) {
    return function(text) {
      return $sce.trustAsHtml(text);
    };
  }

})();
(function () {
  'use strict';

  angular
    .module('msgApp.core', [
        'utils.router',
      ]);

})();
(function() {
  'use strict';

  angular.module('msgApp.home', [
      'msgApp.core',
      'ngDialog',
      'angularFileUpload',
      'toastr'
    ]);
})();
(function() {
  'use strict';

  angular.module('msgApp.login', [
    'msgApp.core',
    'ngCookies',
    'ngAnimate',
    'toastr'
  ]);
})();
(function() {
  'use strict';

  angular.module('msgApp.socket',
    ['msgApp.core']);
  
})();
(function () {
    'use strict';

    angular.module('utils.router', [
        'ui.router'
    ]);

})();
(function() {
  'use strict';

  angular
    .module('msgApp.core')
    .controller('ApplicationController', ApplicationController);

  ApplicationController.$inject = ['$cookies', 'Session'];
  function ApplicationController($cookies, Session) {

    var cookie = $cookies.get('SESSION_VARIABLE');
    
    if (cookie) {
      var sessionVariable = JSON.parse(cookie);
      Session.create(sessionVariable.sessionId, sessionVariable.userId, sessionVariable.nickname);
    }
  }
})();

(function() {
  'use strict';

  angular
    .module('msgApp.core')
    .run(appRun);

    appRun.$inject = ['routerHelper'];
    function appRun(routerHelper) {
      var otherwise = '/404';
      routerHelper.configureStates(getStates(),otherwise);
    }

    function getStates() {
      return [
        {
          state: '404',
          config: {
            url: '/404',
            templateUrl: 'app/core/404.html',
            title: '404'
          }
        }
      ];
    }

})();
(function() {
  angular.module('msgApp.home')
    .service('ChatService', ChatService);

  ChatService.$inject = ['$http', 'Session'];
  function ChatService($http, Session) {
    return {
      getUserHistory: getUserHistory,
      getChannelHistory: getChannelHistory,
      createChannel: createChannel
    };

    function getUserHistory(fromUserId, toUserId) {
      return $http.post('/user/chat', {
        userId: fromUserId,
        toUserId: toUserId,
        sessionId: Session.sessionId
      }).then(function(results) {
        return results.data;
      });
    }

    function getChannelHistory(channelName) {
      return $http.post('/channel/chat', {
        channelName: channelName,
        userId: Session.userId,
        sessionId: Session.sessionId
      }).then(function(results) {
        return results.data;
      });
    }

    function createChannel(channelName) {
      return $http.post('/channel', {
        channelName: channelName,
        userId: Session.userId,
        sessionId: Session.sessionId
      }).then(function(results) {
        return results.data;
      });
    }
  }

})();
(function() {
  'use strict';

  angular.module('msgApp.home')
    .controller('HomeController',HomeController);

  HomeController.$inject = ['$state', 'AuthService', 'ChatService', 'Session', 'Socket', 'ngDialog', 'toastr'];
  function HomeController($state, AuthService, ChatService, Session, socket, ngDialog, toastr) {
    var vm = this;
    init();
    
    // Begin Socket handling

    // Get users
    socket.on('updateUsers', function(users) {
      console.log('users updated', users);
      vm.users = users;
    });

    // Get channels
    socket.on('updateChannels', function(channels) {
      console.log('channels updated', channels);
      vm.channels = channels;
    });

    // TODO: Handle channel users

    // Direct message handling
    socket.on('dm', function(message) {
      if (vm.activeChat.id === message.userId && vm.activeChat.type === 'direct') {
        vm.messages.unshift(message);
      } else {
        var user = findUser(message.userId);
        user.unread = true;
      }
    });

    // Channel message handling
    socket.on('cm', function(message) {
      if (message.nickname === Session.nickname)
        return;
      vm.messages.unshift(message);
    });

    // End socket handling

    // Begin handling Upload file modal window
    vm.uploadFile = function() {
      ngDialog.open({
        templateUrl: 'app/home/modals/fileUpload.template.html',
        controller: 'FileUploadController',
        controllerAs: 'vm'
      }).closePromise.then(function(data) {
        var result = data.value;

        if (typeof result !== 'object')
          return;
        var filename = result.filename;
        var recordId = result.recordId;

        uploadFileNotify(filename, recordId);
      });
    };

    function uploadFileNotify(filename, recordId) {
      if (vm.activeChat.type === 'direct') {
        socket.emit('dm-upload', {
          userId: vm.activeChat.id,
          recordId: recordId,
          filename: filename
        });
      } else {
        // Channel handling
        socket.emit('cm-upload', {
          channel: vm.activeChat.id,
          recordId: recordId,
          filename: filename
        });
      }
      var message = {
          userId: Session.userId,
          nickname: Session.nickname,
          timestamp: Date.now(),
          fileUpload: true
        };
      message.fileMessage = 'File shared: <a href="/download/' + recordId + '">' + filename + '</a>';
      vm.messages.unshift(message);
    }

    vm.prepareChannel = function() {
       ngDialog.open({
        templateUrl: 'app/home/modals/channelCreate.template.html',
        controller: 'ChannelCreateController',
        controllerAs: 'vm'
      }).closePromise.then(function(data) {
        console.log(data);
        if (typeof data.value !== 'object')
          return;
        socket.emit('channelCreated', { channel: data.value.channelName });
        toastr.success('Channel created', 'You did it!');
      });
    };

    vm.createChannel = function() {
      vm.creatingChannel = false;
      if (vm.newChannelName === '')
        return;
      ChatService.createChannel(vm.newChannelName)
        .then(function() {
          // Channel successfully created
          socket.emit('channelCreated', { channel: vm.newChannelName });
          vm.newChannelName = '';
        });
    };

    vm.changeChannel = function(channel) {
      if (vm.activeChat.id === channel.ChannelName)
        return;

      leaveCurrentChannels();

      vm.activeChat = {
        id: channel.ChannelName,
        name: channel.ChannelName,
        type: 'channel'
      };
      vm.messages = [];

      socket.emit('join', { channel: channel.ChannelName });

      ChatService.getChannelHistory(channel.ChannelName)
        .then(function(data) {
          vm.messages = data;
        });
    };

    vm.changeUser = function(user) {
      if (user.userId === Session.userId) {
        toastr.warning('Trying to message yourself, huh?','You can\'t do that');
        return;
      }
      leaveCurrentChannels();
      user.unread = false;

      vm.activeChat = {
        id: user.userId,
        name: user.nickname,
        type: 'direct'
      };
      vm.messages = [];

      ChatService.getUserHistory(Session.userId, user.userId)
        .then(function(data) {
          vm.messages = data;
        });
    };

    vm.sendMessage = function() {
      if (vm.newMessage === '')
        return;

      if (vm.activeChat.id === -1) {
        vm.newMessage = '';
        return;
      }

      if (vm.activeChat.type === 'direct') {
        socket.emit('dm', {
          userId: vm.activeChat.id,
          message: vm.newMessage
        });
      } else {
        socket.emit('cm', {
          channel: vm.activeChat.id,
          message: vm.newMessage
        });
      }

      vm.messages.unshift({
        userId: Session.userId,
        nickname: Session.nickname,
        timestamp: Date.now(),
        message: vm.newMessage
      });
      vm.newMessage = '';  
    };

    vm.logout = function() {
      Session.destroy();
      socket.destroy();
      $state.go('login');
    };

    function findUser(userId) {
      var index = vm.users.map(function(u) { return u.userId; }).indexOf(userId);
      return vm.users[index];
    }

    function leaveCurrentChannels() {
      if (vm.activeChat.type !== 'channel')
        return;

      socket.emit('leave', { channel: vm.activeChat.id });
    }

    function init() {
      vm.activeChat = {
        id: -1,
        name: 'No chat is active',
        type: 'none'
      };
      vm.online = false;
      vm.newMessage = '';

      if (!AuthService.isAuthenticated()) {
        console.log('not authenticated!');
        $state.go('login');
        return;
      }

      socket.init();
      socket.on('connect', function() {
        console.log('connected');
        vm.online = true;
        socket.emit('authentication', { userId: Session.userId, sessionId: Session.sessionId });
      });
      socket.on('unauthorized', function(err) {
        toastr.error('Your session could not be authorized, please log back in','You have been logged off');
        Session.destroy();
        $state.go('login');
      });
      socket.on('disconnect', function(err) {
        vm.online = false;
      });
      socket.on('authenticated', function(data) {
        console.log('success!');
      });
    }

    function isNumeric(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }
  }
})();
(function() {
  'use strict';

  angular
    .module('msgApp.home')
    .run(appRun);

    appRun.$inject = ['routerHelper'];
    function appRun(routerHelper) {
      routerHelper.configureStates(getStates());
    }

    function getStates() {
      return [
        {
          state: 'home',
          config: {
            url: '/home',
            templateUrl: 'app/home/home.template.html',
            controller: 'HomeController',
            controllerAs: 'vm'
          }
        }
      ];
    }
    
})();
(function() {
  'use strict';

  angular.module('msgApp.home')
    .controller('ChannelCreateController', ChannelCreateController);

  ChannelCreateController.$inject = ['$http', '$scope', 'ChatService', 'toastr'];
  function ChannelCreateController($http, $scope, ChatService, toastr) {
    var vm = this;

    vm.createChannel = function() {
      ChatService.createChannel(vm.channelName)
        .then(function() {
          $scope.closeThisDialog({ channelName: vm.channelName });
        })
        .catch(function(err) {
          console.log('error:', err);
          var message = err.data && err.data.message ? err.data.message : 'An unknown error has occurred';
          toastr.error(message,'Uh oh');
        });
    };
  }
})();
(function() {
  'use strict';

  angular.module('msgApp.home')
    .controller('FileUploadController', FileUploadController);

  FileUploadController.$inject = ['$http', '$scope', 'FileUploader'];
  function FileUploadController($http, $scope, FileUploader) {
    var vm = this;

    var uploader = vm.uploader = new FileUploader({
      url: '/upload'
    });

    uploader.onBeforeUploadItem = function(item) {
      console.log('in before');
    };

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
      console.log('in success');
      console.log('response',response);
      $scope.closeThisDialog({ recordId: response.recordId, filename: fileItem.file.name });
    };

    uploader.onErrorItem = function(fileItem, response, status, headers) {
      console.log('in error');
    };
  }
})();
(function() {
  'use strict';

  angular.module('msgApp.login')
    .factory('AuthService', AuthService);

  AuthService.$inject = ['$http', 'Session'];

  function AuthService($http, Session) {
    var authService = {};

    authService.login = function(credentials) {
      console.log('login called..');
      return $http
        .post('/login', credentials)
        .then(function(res) {
          var user = res.data.user;
          console.log(user);
          Session.create(user.sessionId, user.userId, user.nickname);
          console.log('isAuthenticated:',!!Session.userId);
          return user;
        });
    };

    authService.register = function(credentials) {
      return $http
        .post('/register', credentials)
        .then(function(res) {
          console.log(res);
          var user = res.data.user;
          console.log(user);
          Session.create(user.sessionId, user.userId, user.nickname);
          console.log('isAuthenticated:',!!Session.userId);
          return user;
        });
    };

    authService.isAuthenticated = function() {
      console.log('is authenticated called');
      return !!Session.userId;
    };

    return authService;
  }

})();
(function() {
  'use strict';

  angular
    .module('msgApp.login')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['$state', 'AuthService', 'toastr'];
  function LoginController($state, AuthService, toastr) {
    var vm = this;
    this.credentials = {};

    if (AuthService.isAuthenticated()) {
      $state.go('home');
    }

    this.login = function(credentials) {
      AuthService.login(credentials)
        .then(function(user) {
          $state.go('home');
        },
        function(error) {
          if (error.data && error.data.message) {
            toastr.error(error.data.message, 'Could not log in');
          } else {
            toastr.error('An unexpected error has occurred. Please try again', 'Could not log in');
          }
        });
    };
  }

})();
(function() {
  'use strict';

  angular
    .module('msgApp.login')
    .run(appRun);

    appRun.$inject = ['routerHelper'];
    function appRun(routerHelper) {
      routerHelper.configureStates(getStates());
    }

    function getStates() {
      return [
        {
          state: 'root',
          config: {
            url: '/',
            templateUrl: 'app/login/login.template.html',
            controller: 'LoginController',
            controllerAs: 'vm'
          }
        },
        {
          state: 'login',
          config: {
            url: '/login',
            templateUrl: 'app/login/login.template.html',
            controller: 'LoginController',
            controllerAs: 'vm'
          }
        },
        {
          state: 'register',
          config: {
            url: '/register',
            templateUrl: 'app/login/register.template.html',
            controller: 'RegisterController',
            controllerAs: 'vm'
          }
        }
      ];
    }
    
})();
(function() {
  'use strict';

  angular.module('msgApp.login')
    .controller('RegisterController', RegisterController);

  RegisterController.$inject = ['$state', 'AuthService', 'toastr'];
  function RegisterController($state, AuthService, toastr) {
    var vm = this;

    vm.register = function(credentials) {
      AuthService.register(credentials)
        .then(function(user) {
          toastr.success('You have been registered successfully!', 'Welcome!');
          $state.go('home');
          return user;
        })
        .catch(function(err) {
          if (err.data && err.data.message) {
            toastr.warning('Could not register: ' + err.data.message,'Registration Failure');
          } else {
            toastr.warning('Could not register: Unknown Failure', 'Registration Failure');
          }
        });
    };
  }
})();
(function() {
  'use strict';

  angular.module('msgApp.login')
    .service('Session', Session);

  Session.$inject = [ '$cookies' ];
  function Session($cookies) {

    this.create = function createSession(sessionId, userId, nickname) {
      this.sessionId = sessionId;
      this.userId = userId;
      this.nickname = nickname;
      var cookie = JSON.stringify({ sessionId: sessionId, userId: userId, nickname: nickname });
      $cookies.put('SESSION_VARIABLE', cookie);
    };

    this.destroy = function destroySession() {
      this.sessionId = null;
      this.userId = null;
      $cookies.remove('SESSION_VARIABLE');
    };
  }
})();
(function() {
  'use strict';

  angular.module('msgApp.socket')
    .factory('Socket',Socket);

  Socket.$inject = ['$rootScope'];
  function Socket($rootScope) {
    var socket;
    var existingEvents = [];
    var isDestroying = false;
    return {
      on: function (eventName, callback) {
        existingEvents.push(eventName);
        socket.on(eventName, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            callback.apply(socket, args);
          });
        });
      },
      emit: function (eventName, data, callback) {
        socket.emit(eventName, data, function () {
          var args = arguments;
          $rootScope.$apply(function () {
            if (callback) {
              callback.apply(socket, args);
            }
          });
        });
      },
      init: function() {
        console.log('in init');
        socket = io.connect({
          'force new connection': true
        });
      },
      destroy: function() {
        isDestroying = true;
        existingEvents.forEach(function(event) {
          socket.removeEventListener(event);
        });
        socket.disconnect();
        socket.close();
        socket = null;
      }
    };
  }
})();
// From: https://github.com/johnpapa/angular-styleguide#routing
// Copyright (c) 2014-2015 John Papa

// (The MIT License)
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the 'Software'),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

(function() {
  'use strict';
  angular
    .module('utils.router')
    .provider('routerHelper', routerHelperProvider);

  routerHelperProvider.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider' ];
  /* @ngInject */
  function routerHelperProvider($locationProvider, $stateProvider, $urlRouterProvider) {
    /* jshint validthis:true */
    this.$get = RouterHelper;

    $locationProvider.html5Mode(true);

    RouterHelper.$inject = ['$state'];
    /* @ngInject */
    function RouterHelper($state) {
      var hasOtherwise = false;

      var service = {
        configureStates: configureStates,
        getStates: getStates
      };

      return service;

      ///////////////

      function configureStates(states, otherwisePath) {
        states.forEach(function(state) {
          $stateProvider.state(state.state, state.config);
        });
        if (otherwisePath && !hasOtherwise) {
          hasOtherwise = true;
          $urlRouterProvider.otherwise(otherwisePath);
        }
      }

      function getStates() {
        return $state.get();
      }
    }
  }
})();