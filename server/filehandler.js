var q = require('q');
var connection = require('../config/mysql');

exports.uploadFile = function(req, res) {
  if (!req.files)
    return res.status(400).json({ message: 'No file uploaded '});

  var savedFilename = req.files.file[0].name;
  var originalFilename = req.files.file[0].originalname;
  saveFileDbRecord(savedFilename, originalFilename)
    .then(function(recordId) {
      console.log('recordId', recordId);
      return res.status(200).json({ message: 'successful upload', recordId: recordId });
    })
    .catch(function(err) {
      return res.status(500).json({ message: 'An unexpected error has occurred', error: err });
    });
};

exports.downloadFile = function(req, res) {
  var recordId = req.params.recordId;
  if (!recordId)
    return res.status(400).json({ message: 'No file provided' });

  getFilenameFromRecordId(recordId)
    .then(function(fileArray) {
      var originalFilename = fileArray[0];
      var savedFilename = fileArray[1];

      res.download(__dirname + '/../uploads/' + savedFilename, originalFilename);
    })
    .catch(function(err) {
      return res.status(404).json({ message: 'File not found' });
    });
};

function saveFileDbRecord(savedFilename, originalFilename) {
  var deferred = q.defer();
  var strSql =  'INSERT INTO msgapp.uploads ' +
                '(SavedFilename, OriginalFilename) ' +
                'VALUES (?, ?); ' +
                'SELECT LAST_INSERT_ID() as RecordId';

  connection.query(strSql, [ savedFilename, originalFilename ],
    function(err, rows) {
      if (err)
        deferred.reject(err);
      else {
        deferred.resolve(rows[1][0].RecordId);
      }
    });

  return deferred.promise;
}

function getFilenameFromRecordId(recordId) {
  var deferred = q.defer();
  var strSql =  'SELECT OriginalFilename, SavedFilename ' +
                'FROM msgapp.uploads ' +
                'WHERE UploadId = ?;';

  connection.query(strSql, [ recordId ],
    function(err, rows) {
      if (err)
        deferred.reject(err);
      else {
        deferred.resolve([ rows[0].OriginalFilename, rows[0].SavedFilename ]);
      }
    });

  return deferred.promise;
}