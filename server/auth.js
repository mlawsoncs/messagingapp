var connection = require('../config/mysql');
var crypto = require('crypto');
var password = require('./password');
var q = require('q');

// Returns the nickname of the user who is authenticated
exports.authenticate = function(userId, sessionId) {
  var deferred = q.defer();
  var strSql =  'SELECT Nickname ' +
                'FROM msgapp.users ' +
                'WHERE UserId = ? ' +
                '  AND SessionId = ?;';
  connection.query(strSql, [userId, sessionId],
      function(err, rows) {
        if (err || rows.length === 0) {
          deferred.reject('Could not authenticate');
        }
        else {
          deferred.resolve(rows[0].Nickname);
        }
      });
  return deferred.promise;
};

exports.login = function(req, res) {
  var strSql = 
  connection
    .query('select PasswordHash, UserId ' +
            'from msgapp.users ' +
            'where Username = ?', [ req.body.username ],
              function(err, rows) {
                if (err) {
                  return res.status(500).json(err);
                }
                if (rows.length === 0) {
                  return res.status(403).json({ message: 'Failed username/password'});
                }
                // Validate password hash
                var hash = rows[0].PasswordHash;
                var userId = rows[0].UserId;
                if (password.validate(hash, req.body.password)) {
                 setSessionKey(userId, connection)
                  .then(function(user) {
                    if (!user) {
                      return res.status(500).json({ message: 'An unexpected database error has occurred;'});
                    }
                    return res.status(200).json({ user: user, message: 'You have been logged in.' });
                  });
                } else {
                  return res.status(403).json({ message: 'Failed username/password'});
                }
              });
};

exports.register = function(req, res) {
  var username = req.body.username,
    rawPassword = req.body.password,
    email = req.body.email,
    nickname = req.body.displayName,
    passFail = -1;

    if (!username || !password || !email || !nickname) {
      return res.status(400).json({ message: 'Incomplete registration' });
    }

    connection
    .query('CALL msgapp.CreateNewUser(?, ?, ?, ?);',
      [ username, nickname, password.hash(rawPassword), email ],
      function(err, rows, fields) {
        if (err) {
          console.error(err);
          return res.status(500).json(err);
        } else {
          passFail = rows[1][0].PassFail;
          userId = rows[1][0].UserId;
          if (passFail !== 1) {
            return res.status(500).json({ message: 'A user already exists with these credentials '});
          } else {
            setSessionKey(userId, connection)
              .then(function(user) {
                if (!user) {
                  return res.status(500).json({ message: 'An unexpected database error has occurred;'});
                }
                return res.status(200).json({ user: user, message: 'You have been logged in.' });
              })
              .catch(function(err) {
                return res.status(500).json({ message: err });
              });
          }
        }
      }); // end CreateNewUser
};

var setSessionKey = function(userId, conn) {
  var key = generateKey();
  var deferred = q.defer();
  conn.query('UPDATE msgapp.users ' +
    'SET SessionId = ? ' +
    'WHERE UserId = ?; ' +
    'SELECT Username, Nickname, EmailAddress ' +
    'FROM msgapp.users ' + 
    'WHERE UserId = ?',
    [ key, userId, userId ],
    function(err, rows) {
      if (err) {
        console.log('An error has occurred in the update', err);
        deferred.reject(err);
        return;
      }

      var results = rows[1][0];
      var username = results.Username;
      var nickname = results.Nickname;
      var email = results.EmailAddress;
      var user = {
        sessionId: key,
        userId: userId,
        username: username,
        nickname: nickname,
        email: email
      };
      
      deferred.resolve(user);
    });
  return deferred.promise;
};



var generateKey = function() {
  var sha = crypto.createHash('sha256');
  sha.update(Math.random().toString());
  return sha.digest('hex');
};