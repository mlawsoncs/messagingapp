var auth = require('./auth');
var q = require('q');
var connection = require('../config/mysql');

exports.getChatHistory = function(req, res) {
  var originId = req.body.userId;
  var sessionId = req.body.sessionId;
  var destId = req.body.toUserId;

  var authPromise = auth.authenticate(originId, sessionId);
  var chatHistory = getChatHistory(originId, destId);

  q.allSettled([ authPromise, chatHistory ])
    .then(function(results) {
      if (results[0].state !== 'fulfilled') {
        return res.status(403).json({ message: 'Forbidden' });
      }
      if (results[1].state !== 'fulfilled') {
        console.log(results[1].reason);
        return res.status(500).json(results[1].reason);
      }
      return res.status(200).json(buildFileMessage(results[1].value));
    })
    .catch(function(err) {
      console.log('error: ', err);
      return res.status(500).json(results[1].reason);
    });
};

exports.saveMessage = function(fromId, toId, message, uploadId) {
  var deferred = q.defer();
  var sqlStr =  'INSERT INTO msgapp.chathistory ' +
                '(UserFrom, ' +
                'UserTo, ' +
                'Message, ' +
                'UploadId) ' +
                'VALUES (?, ?, ?, ?)';
  connection.query(sqlStr, [ fromId, toId, message, uploadId ],
    function(err, rows) {
      if (err) {
        deferred.reject(err);
      } else {
        deferred.resolve(true);
      }
    });
  return deferred.promise;
};

function getChatHistory(from, to) {
  var deferred = q.defer();
  var sqlStr =  'SELECT ' +
                '    ch.Message as message, ' +
                '    ch.Timestamp as timestamp, ' +
                '    u.Nickname as nickname, ' +
                '    up.OriginalFilename as filename, ' +
                '    up.UploadId as recordId ' +
                'FROM msgapp.chathistory as ch ' +
                'LEFT JOIN msgapp.users as u ' +
                '    ON ch.UserFrom = u.UserId ' +
                'LEFT JOIN msgapp.uploads as up ' +
                '    ON ch.UploadId = up.UploadId ' +
                'WHERE (UserFrom = ? AND UserTo = ?) ' +
                '  OR  (UserFrom = ? AND UserTo = ?) ' +
                'ORDER BY timestamp desc;';

  connection.query(sqlStr, [from, to, to, from ],
    function(err, rows) {
      if (err) {
        deferred.reject(err);
      } else {
        deferred.resolve(rows);
      }
    });
  return deferred.promise;
}

function buildFileMessage(messages) {
  return messages.map(function(message) {
    if (message.recordId !== null) {
      message.fileMessage = 'File shared: <a href="/download/' + message.recordId + '">' + message.filename + '</a>';
    }
    return message;
  });
}