module.exports = function(http) {
  var io = require('socket.io')(http);
  var auth = require('./auth');
  var userchat = require('./userchat');
  var channelchat = require('./channelchat');

  var onlineUsers = [];
  var clients = [];
  var channelClients = {};

  io.on('connection', function(socket) {
    socket.auth = false;
    socket.username = null;
    console.log('incoming connection..');

    // Authentication handling
    socket.on('authentication', function(data) {
      auth.authenticate(data.userId, data.sessionId)
        .then(function(result) {
          // Authentication success
          socket.auth = true;
          socket.nickname = result;
          socket.userId = data.userId;
          newConnection(socket);
          socket.emit('authenticated');
        })
        .catch(function(err) {
          console.log('error:', err);
          // Authentication failure
          socket.emit('unauthorized', 'You could not be authenticated');
          socket.disconnect('unauthorized');
        });
    });

    // Direct message handling
    socket.on('dm', function(data) {
      var toSocketId = clients.map(function(c) { return c.userId; }).indexOf(data.userId);
      if (toSocketId === -1) {
        socket.emit('messageFailed', { message: 'That user is not logged in.'});
      }

      // saving message
      userchat.saveMessage(socket.userId, data.userId, data.message)
        .then(function() {
          socket.emit('messageSent', data);
          var toSocket = clients[toSocketId];
          toSocket.emit('dm', {
            userId: socket.userId,
            nickname: socket.nickname,
            timestamp: Date.now(),
            message: data.message
          });
        })
        .catch(function(err) {
          socket.emit('messageFailed', { message: 'An error has occurred', error: err });
        });
    });

    // File upload notification
    socket.on('dm-upload', function(data) {
      console.log('Upload attempted..');
      var toSocketId = clients.map(function(c) { return c.userId; }).indexOf(data.userId);
      if (toSocketId === -1) {
        console.log('message failed');
        socket.emit('messageFailed', { message: 'That user is not logged in.'});
      }
      userchat.saveMessage(socket.userId, data.userId, null, data.recordId)
        .then(function() {
          console.log('upload saved!');
          var toSocket = clients[toSocketId];
          var message = 'File shared: <a href="/download/' + data.recordId + '">' + data.filename + '</a>';

          toSocket.emit('dm', {
            userId: socket.userId,
            nickname: socket.nickname,
            timestamp: Date.now(),
            fileMessage: message,
            fileUpload: true
          });
          // Saving uploads?
          socket.emit('uploadSent', data);
        })
        .catch(function(err) {
          console.log('error', err);
          socket.emit('uploadFailed', { message: 'An error has occurred', error: err });
        });
    });

    // Channel message handling

    // Joining a channel
    socket.on('join', function(data) {
      var channelName = data.channel;

      channelchat
        .isChannel(channelName)
        .then(function(channelName) {
          // Check for existing channels
          if (socket.channel) {
            io.to(socket.channel).emit('part', { nickname: socket.nickname });
            var index = channelClients[channelName].indexOf(socket.nickname);
            channelClients.splice(index, 1);
          }

          // Send notification to others
          socket.join(channelName);
          io.to(channelName).emit('join', { nickname: socket.nickname });

          // Client list handling
          socket.channel = channelName;
          if (!channelClients[channelName]) {
            channelClients[channelName] = [socket.nickname];
          } else {
            channelClients[channelName].push(socket.nickname);
          }

          // Send current user list
          socket.emit('channelUsers', { channel: channelName, users: channelClients[channelName] });
        })
        .catch(function(error) {
          socket.emit('joinFailed', error);
        });
    });

    // Leaving a channel
    socket.on('leave', function(data) {
      var channelName = data.channel;
      socket.channel = null;
      io.to(socket.channel).emit('part', { nickname: socket.nickname });
      socket.leave(channelName);
    });

    // Channel creation
    socket.on('channelCreated', function(data) {
      channelchat
        .getChannels()
        .then(function(channels) {
          // send to all users
          io.emit('updateChannels', channels);
        });
    });

    // Messaging to a channel
    socket.on('cm', function(data) {
      var channelName = data.channel;
      var message = data.message;

      // TODO: sanitizing user input
      if (socket.channel !== channelName) {
        socket.emit('messageFailed', { message: 'You are not on that channel' });
        return;
      }
      channelchat.saveMessage(socket.userId, socket.channel, data.message)
        .then(function() {
          socket.emit('messageSent', data);
          io.to(channelName).emit('cm', { nickname: socket.nickname, message: message, timestamp: Date.now() });
        })
      .catch(function(err) {
          console.log('error', err);
          socket.emit('messageFailed', { message: 'An error has occurred', error: err });
        });
    });

    // File upload notification
    socket.on('cm-upload', function(data) {
      var channelName = data.channel;
      
      if (socket.channel !== channelName) {
        socket.emit('messageFailed', { message: 'You are not on that channel' });
        return;
      }
      channelchat.saveMessage(socket.userId, socket.channel, null, data.recordId)
        .then(function() {
          var message = 'File shared: <a href="/download/' + data.recordId + '">' + data.filename + '</a>';

          io.to(channelName).emit('cm', {
            nickname: socket.nickname,
            timestamp: Date.now(),
            fileMessage: message,
            fileUpload: true
          });

          socket.emit('uploadSent', data);
        })
        .catch(function(err) {
          console.log('error', err);
          socket.emit('uploadFailed', { message: 'An error has occurred', error: err });
        });
    });

    // Disconnect after 1000 if not authorized
    setTimeout(function() {
      if (!socket.auth) {
        console.log('Disconnecting socket', socket.id);
        socket.emit('unauthorized', 'You could not be authenticated');
        socket.disconnect('unauthorized');
      }
    }, 1000);

    // disconnection logic
    socket.on('disconnect', function() {
      // Check for channel list
      if (socket.channel) {
        io.to(socket.channel).emit('part', { nickname: socket.nickname });
        var index = channelClients[socket.channel].indexOf(socket.nickname);
        channelClients[socket.channel].splice(index, 1);
      }

      removeUser(socket.userId, null, false);
    });

  });

  function newConnection(socket) {
    removeUser(socket.userId, socket.id, true); // To prevent duplicate entries
    
    onlineUsers.push({ userId: socket.userId, nickname: socket.nickname });
    clients.push(socket);
    io.emit('updateUsers', onlineUsers);

    // send current channel list
    // TODO: Error handling
    channelchat
      .getChannels()
      .then(function(channels) {
        socket.emit('updateChannels', channels);
      });
  }

  function removeUser(userId, socketId, removeExisting) {
    // Remove from online Users
    var index = onlineUsers.map(function(r) { return r.userId; }).indexOf(userId);
    if (index !== -1) 
      onlineUsers.splice(index, 1);

    // Remove from clients
    if (removeExisting) {
      var newClients = clients.filter(function(client, index) {
        if (client.userId === userId && client.id !== socketId) {
          console.log('userId:', userId);
          console.log('socketIds', socketId, client.id);
          client.emit('unauthorized', 'Another user has connected');
          client.disconnect();
        } else {
          return client;
        }
      });
      clients = newClients;
    } else {
      // Send update to all users
      io.emit('updateUsers', onlineUsers);
    }
  }
};