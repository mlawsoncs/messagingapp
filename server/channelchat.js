var auth = require('./auth');
var q = require('q');
var connection = require('../config/mysql');

exports.createChannel = function(req, res) {
  var channelName = req.body.channelName;
  var sessionId = req.body.sessionId;
  var userId = req.body.userId;

  var authPromise = auth.authenticate(userId, sessionId);
  var makeChannelPromise = makeChannel(channelName);

  q.allSettled([ authPromise, makeChannelPromise ])
    .then(function(results) {
      if (results[0].state !== 'fulfilled') {
        return res.status(403).json({ message: 'Forbidden' });
      }
      if (results[1].state !== 'fulfilled') {
        var error = results[1].reason;
        var message = 'Unknown error';
        if (error.code === 'ER_DUP_ENTRY') {
          message = 'A channel with that name already exists';
        }
        return res.status(500).json({ message: message });
      }
      return res.status(200).json({ message: 'Channel created' });
    });
};

exports.getChatHistory = function(req, res) {
  var channelName = req.body.channelName;
  var userId = req.body.userId;
  var sessionId = req.body.sessionId;

  var authPromise = auth.authenticate(userId, sessionId);
  var chatHistory = getChatHistory(channelName);

  q.allSettled([ authPromise, chatHistory ])
    .then(function(results) {
      if (results[0].state !== 'fulfilled') {
        return res.status(403).json({ message: 'Forbidden' });
      }
      if (results[1].state !== 'fulfilled') {
        console.log('error', results[1].reason);
        return res.status(500).json(results[1].reason);
      }
      return res.status(200).json(buildFileMessage(results[1].value));
    });

};

function getChatHistory(channelName) {
  var deferred = q.defer();
  var sqlStr =  'SELECT ' +
                '    ch.Message as message, ' +
                '    ch.Timestamp as timestamp, ' +
                '    u.Nickname as nickname, ' +
                '    up.OriginalFilename as filename, ' +
                '    up.UploadId as recordId ' +
                'FROM msgapp.chathistory as ch ' +
                'LEFT JOIN msgapp.users as u ' +
                '    ON ch.UserFrom = u.UserId ' +
                'LEFT JOIN msgapp.channels as c ' +
                '    ON ch.ChannelTo = c.ChannelId ' +
                 'LEFT JOIN msgapp.uploads as up ' +
                '    ON ch.UploadId = up.UploadId ' +
                'WHERE c.ChannelName = ? ' +
                'ORDER BY timestamp desc;';

 connection.query(sqlStr, [ channelName ],
  function(err, rows) {
    if (err) {
      deferred.reject(err);
    } else {
      deferred.resolve(rows);
    }
  });
  return deferred.promise;
}

exports.saveMessage = function(userId, channelName, message, uploadId) {
  var deferred = q.defer();
  var sqlStr =  'INSERT INTO msgapp.chathistory ' +
                '(UserFrom, ' +
                'ChannelTo, ' +
                'Message, ' +
                'UploadId) ' +
                'VALUES (?, ?, ?, ?)';

  getChannelIdFromName(channelName)
    .then(function(channelId) {
      connection.query(sqlStr, [userId, channelId, message, uploadId],
        function(err, rows) {
          if (err) {
            deferred.reject(err);
          } else {
            deferred.resolve(true);
          }
        });
    });
  return deferred.promise;
};

exports.getChannels = function() {
  var deferred = q.defer();
  var strSql =  'SELECT ChannelName ' +
                'FROM msgapp.channels';
  connection.query(strSql,
    function(err, rows) {
      if (err)
        deferred.reject(err);
      else
        deferred.resolve(rows);
    });

  return deferred.promise;
};

exports.isChannel = function(channelName) {
  var deferred = q.defer();
  var strSql =  'SELECT ChannelName ' +
                'FROM msgapp.channels ' +
                'WHERE ChannelName = ?';
  connection.query(strSql, [ channelName ],
    function(err, rows) {
      if (err)
        deferred.reject({ message: 'An unexpected error has occurred.', error: err });
      else if (!rows[0].ChannelName)
        deferred.reject({ message: 'Channel does not exist' });
      else
        deferred.resolve(channelName);
    });

  return deferred.promise;
};

function makeChannel(channelName) {
  var deferred = q.defer();
  var strSql =  'INSERT INTO msgapp.channels ' +
                '(ChannelName, ' +
                'Active) ' +
                'VALUES ' +
                '(?, 1);';
  connection.query(strSql, [ channelName ],
    function(err, rows) {
      if (err)
        deferred.reject(err);
      else
        deferred.resolve(true);
    });

  return deferred.promise;
}

function getChannelIdFromName(channelName) {
  var deferred = q.defer();
  var strSql =  'SELECT ChannelId ' +
                'FROM msgapp.channels ' +
                'WHERE ChannelName = ?;';
  connection.query(strSql, [ channelName ],
    function(err, rows) {
      if (err)
        deferred.reject(err);
      else if (rows.length === 0)
        deferred.reject('Channel not found');
      else
        deferred.resolve(rows[0].ChannelId);
    });

  return deferred.promise;
}

function buildFileMessage(messages) {
  return messages.map(function(message) {
    if (message.recordId !== null) {
      message.fileMessage = 'File shared: <a href="/download/' + message.recordId + '">' + message.filename + '</a>';
    }
    return message;
  });
}