CREATE DATABASE `msgapp` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `msgapp`;

/* TABLES */
CREATE TABLE `users` (
  `UserId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Username` varchar(25) NOT NULL,
  `Nickname` varchar(50) NOT NULL,
  `PasswordHash` char(60) DEFAULT NULL,
  `EmailAddress` varchar(254) DEFAULT NULL,
  `SessionId` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `UserId_UNIQUE` (`UserId`),
  UNIQUE KEY `Username_UNIQUE` (`Username`),
  UNIQUE KEY `Nickname_UNIQUE` (`Nickname`),
  UNIQUE KEY `EmailAddress_UNIQUE` (`EmailAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

CREATE TABLE `channels` (
  `ChannelId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ChannelName` varchar(50) DEFAULT NULL,
  `Active` int(11) DEFAULT '0',
  PRIMARY KEY (`ChannelId`),
  UNIQUE KEY `ChannelId_UNIQUE` (`ChannelId`),
  UNIQUE KEY `ChannelName_UNIQUE` (`ChannelName`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `uploads` (
  `UploadId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `OriginalFilename` varchar(100) DEFAULT NULL,
  `SavedFilename` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`UploadId`),
  UNIQUE KEY `UploadId_UNIQUE` (`UploadId`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

CREATE TABLE `chathistory` (
  `ChatHistoryId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserFrom` int(10) unsigned NOT NULL,
  `UserTo` int(10) unsigned DEFAULT NULL,
  `ChannelTo` int(10) unsigned DEFAULT NULL,
  `Message` varchar(256) DEFAULT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UploadId` int(10) unsigned DEFAULT NULL,
  `chathistorycol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ChatHistoryId`),
  UNIQUE KEY `ChatHistoryId_UNIQUE` (`ChatHistoryId`),
  KEY `Channels_ChannelTo_idx` (`ChannelTo`),
  KEY `fk_users_UserTo_idx` (`UserTo`),
  KEY `fk_users_UserFrom_idx` (`UserFrom`),
  KEY `fk_uploads_UploadId` (`UploadId`),
  CONSTRAINT `fk_channels_ChannelTo` FOREIGN KEY (`ChannelTo`) REFERENCES `channels` (`ChannelId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_uploads_UploadId` FOREIGN KEY (`UploadId`) REFERENCES `uploads` (`UploadId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_UserFrom` FOREIGN KEY (`UserFrom`) REFERENCES `users` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_UserTo` FOREIGN KEY (`UserTo`) REFERENCES `users` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

/* END TABLES */

/* STORED PROCEDURES */

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CreateNewUser`(IN UserName VARCHAR(25),
 IN Nickname VARCHAR(50),
 IN PasswordHash CHAR(60),
 IN EmailAddress VARCHAR(254))
BEGIN

  DECLARE EXIT HANDLER FOR SQLEXCEPTION
    SELECT 0 as PassFail, null as UserId;
    
    SET @ExistingUsers = 0;
    
    SELECT @ExistingUsers = count(*)
    FROM msgapp.users
    WHERE `Username` = UserName
    OR    `EmailAddress` = EmailAddress
    OR    `Nickname` = Nickname;

  IF @ExistingUsers <> 0 THEN
    SELECT -1 as PassFail, null as UserId;
  ELSE
      INSERT INTO msgapp.users
    (Username,
    Nickname,
    PasswordHash,
    EmailAddress)
    VALUES
        (UserName,
         NickName,
         PasswordHash,
         EmailAddress);
  
    SELECT 1 as PassFail, LAST_INSERT_ID() as UserId;
    END IF;
END$$
DELIMITER ;

/* END STORED PROCEDURES */