(function() {
  'use strict';
  module.exports = function(grunt) {

    grunt.initConfig({ 
      env: {
        dev: {
          NODE_ENV: 'development'
        }
      },
      jshint: {
        all: ['Gruntfile.js', 'config/**/*.js', 'server/**/*.js', 'client/app/**/*.js', '!client/app/**/*.spec.js']
      },
      // Concat is needed before uglify to put the files in the right order
      concat: {
        build: {
          files: [
            {
              src: ['client/app/**/*.module.js', 'client/app/**/*.js', '!client/app/**/*.spec.js'],
              dest: 'client/app.js'
            }
          ]
        }
      },
      uglify: {
        build: {
          options: {
            sourceMap: true
          },
          files: {
            'client/app.min.js': ['client/app.js']
          }
        }
      },
      nodemon: {
        dev: {
          script: 'server.js'
        }
      },
      watch: {
        js: {
          files: ['Gruntfile.js', 'client/app/**/*.js', '!client/app/**/*.spec.js'],
          tasks: ['jshint', 'concat', 'uglify']
        }
      },
      concurrent: {
        options: {
          logConcurrentOutput: true
        },
        tasks: ['nodemon', 'watch']
      }
    });

    grunt.registerTask('default', ['env', 'jshint', 'concat', 'uglify', 'concurrent']);

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-env');
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-concurrent');
  };
})();