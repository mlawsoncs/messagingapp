/// <reference path="typings/node/node.d.ts"/>
var fs = require('fs');
var app = require('express')();
var http = require('http').Server(app);

// Bootstrap application settings
require('./config/express')(app);
require('./server/socket')(http);

// Bootstrap routes
require('./config/routes')(app);

http.listen(5000, function() {
  console.log('listening on port 5000..');
});
