// TODO: give routes
var express = require('express');
var path = require('path');

var auth = require('../server/auth');
var userchat = require('../server/userchat');
var channelchat = require('../server/channelchat');
var filehandler = require('../server/filehandler');

// connection.connect();

module.exports = function(app) {

  // app.get('/api/test', function(req, res) {
  //   connection.query('select 1 + 1 as solution', function(err, rows, fields) {
  //     console.log('The solution is: ', rows[0].solution);
  //   });
  // });

  app.post('/login',auth.login);
  app.post('/register',auth.register);

  app.post('/user/chat', userchat.getChatHistory);

  app.post('/channel', channelchat.createChannel);
  app.post('/channel/chat', channelchat.getChatHistory);

  app.post('/upload', filehandler.uploadFile);
  app.get('/download/:recordId', filehandler.downloadFile);

  app.use(express.static(path.resolve(__dirname + '/../client')));
  app.use('*',function(req, res, next) {
    var publicPath = path.resolve(__dirname + '/../client/index.html');
    res.sendFile(publicPath);
  });
};