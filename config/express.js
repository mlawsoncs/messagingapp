var express = require('express');
var path = require('path');
var logger = require('morgan');
var multer = require('multer');
var bodyParser = require('body-parser');

// No views are being used with this app

module.exports = function(app) {
  app.use(logger('dev'));
  app.use(multer({ dest: './uploads', putSingleFilesInArray: true }));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ 
    extended: true
  }));
};