var mysql = require('mysql');
var connection = mysql.createConnection({ 
  host: 'localhost',
  user: 'appAdmin',
  password: 'YOUR_PASSWORD',
  multipleStatements: true
});

connection.connect();

module.exports = connection;
