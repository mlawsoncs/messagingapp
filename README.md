# README #

This is a chat application built on top of Node, Angular, and MySQL

## Features ##

* User registration
* Channels/Rooms
* File uploading

## Installation ##

Installation is fairly simple. First, run the database script provided in the schema's folder on your MySQL instance. Then, view the mysql.js file in the config folder to make sure it's properly configured.

* Install dependencies: `npm install` 
* Run: `grunt`